# Propeller GCC Builder
This is a Docker image for building Propeller GCC in an easily re-creatable environment.
To build Propeller GCC just run the "build-propgcc.sh" script.