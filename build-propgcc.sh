#!/bin/bash
#
# Build Propeller GCC (Using Docker)
#
# This script builds Propeller GCC from source using the gcc:4 Docker image.
# The output is stored in a folder named "parallax" and should be moved to "/opt/parallax"
#
# UPDATING YOUR PATH
# Once you have created "/opt/parallax" you can also add the created binaries to your path.
# You need to add "/opt/parallax/bin" to your path.
#

# Clone the source
#git clone git@github.com:parallaxinc/propgcc.git

# Make the directories to hold the output
#mkdir parallax

# Build prop-gcc
docker run -it --rm -v `pwd`/propgcc:/propgcc -v `pwd`/parallax:/opt/parallax prop-build
