FROM gcc:4

# Install dependencies
RUN apt-get update && apt-get install -y bison

# Install an older version of texinfo
RUN wget http://ftp.gnu.org/gnu/texinfo/texinfo-4.13a.tar.gz && \
	tar -zxvf texinfo-4.13a.tar.gz && \
	cd texinfo-4.13 && \
	./configure && make && make install && \
	cd .. && \
	rm -r texinfo-4.13 && rm texinfo-4.13a.tar.gz

# Set up build environemnt variables
ENV CFLAGS="-Wno-deprecated-declarations -Wno-empty-body -Wno-unused-value -Wno-sizeof-pointer-memaccess"
ENV PATH="$PATH:/opt/parallax/bin"

# Source mount location
VOLUME /propgcc
WORKDIR /propgcc

# Output location
VOLUME /opt/parallax

# Default build command
CMD make
